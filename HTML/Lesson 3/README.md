# HTML Lesson 1
## _Workshop: HTML Lesson 1_

[![N|Solid](https://i.postimg.cc/htYRKKsH/images.png)](https://www.postgresql.org)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is a first lesson
## Topics

- HTML DOM Structure
- Headings
- Paragraphs 
- Tables
- Lists
- Forms
- Images

The HyperText Markup Language, or HTML is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS) and scripting languages such as JavaScript, etc.

## Tools

The below tools are required for the deployment:

- [Ok Web server] - Lightweight web server extension to Chrome!
- [Docker] - In case if you need containers
- [Sublime Text] - HTML Editor for writing codes.

## Installation

Deployment of html code:

```sh
You need to add project folder in the web server.
```

Install the docker image - only incase if you want docker containers.

```sh
docker build . -t <your username>/lesson1-html
```

For running container...

```sh
docker run -p 8080:80 -d <your username>/lesson1-html
```

Curl localhost
```sh
curl -i http://127.0.0.1:8080
```
